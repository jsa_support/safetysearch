export class FormData {
    fullname = '';
    email  = '';
    phone = '';
    title = '';
    password = '';
    companyName = '';
    industry = '';
    companySize = '';
    requirementType = '';
    requirementName = '';
    searchCourse = '';

    clear() {
        this.fullname = '';
        this.email = '';
        this.phone = '';
        this.title = '';
        this.password = '';
        this.companyName = '';
        this.industry = '';
        this.companySize = '';
        this.requirementType = '';
        this.requirementName  = '';
        this.searchCourse = '';
    }
}

export class Personal {
    fullname = '';
    email = '';
    phone = '';
    title = '';
    password = '';
}

export class Company {
    companyName = '';
    industry = '';
    companySize = '';
}

export class Work {
    requirementType = '';
    searchCourse = '';
    requirementName = '';
}

export class Address {
    street = '';
    city = '';
    state = '';
    zip = '';
}
