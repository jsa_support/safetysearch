import { Injectable } from '@angular/core';
import { FormData, Personal, Address, Work, Company } from './formData.model';
import { WorkflowService } from '../workflow/workflow.service';
import { STEPS } from '../workflow/workflow.model';

@Injectable()
export class FormDataService {
    private formData: FormData = new FormData();
    private isPersonalFormValid = false;
    private isCompanyFormValid = false;
    private isWorkFormValid = false;
    private isAddressFormValid = false;

    constructor(private workflowService: WorkflowService) {    }

    getPersonal(): Personal {
        // Return the Personal data
        const personal: Personal = {
            fullname: this.formData.fullname,
            email: this.formData.email,
            phone: this.formData.phone,
            title: this.formData.title,
            password: this.formData.password
        };
        return personal;
    }


    setPersonal(data: Personal) {
        // Update the Personal data only when the Personal Form had been validated successfully
        this.isPersonalFormValid = true;
        this.formData.fullname = data.fullname;
        this.formData.email = data.email;
        this.formData.phone = data.phone;
        this.formData.title = data.title;
        this.formData.password = data.password;
        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.personal);
    }

    getCompany(): Company {
        // Return the Personal data
        const company: Company = {
            companyName: this.formData.companyName,
            industry: this.formData.industry,
            companySize: this.formData.companySize
        };
        return company;
    }
    setCompany(data: Company) {
        // Update the Personal data only when the Personal Form had been validated successfully
      this.isCompanyFormValid = true;
        this.formData.companyName = data.companyName;
        this.formData.industry = data.industry;
        this.formData.companySize = data.companySize;
        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.company);
    }

    getWork(): Work {
        // Return the work type
        const work: Work  =  {
            requirementName: this.formData.requirementName,
            requirementType: this.formData.requirementType,
          searchCourse: this.formData.searchCourse
        };
        return work;
    }
    setWork(data: Work) {
        // Update the work type only when the Work Form had been validated successfully
        this.isWorkFormValid = true;
        this.formData.requirementName = data.requirementName;
        this.formData.requirementType = data.requirementType;
        // Validate Work Step in Workflow
        this.workflowService.validateStep(STEPS.work);
    }
/*
    getAddress() : Address {
        // Return the Address data
        var address: Address = {
            street: this.formData.street,
            city: this.formData.city,
            state: this.formData.state,
            zip: this.formData.zip
        };
        return address;
    }

    setAddress(data: Address) {
        // Update the Address data only when the Address Form had been validated successfully
        this.isAddressFormValid = true;
        this.formData.street = data.street;
        this.formData.city = data.city;
        this.formData.state = data.state;
        this.formData.zip = data.zip;
        // Validate Address Step in Workflow
        this.workflowService.validateStep(STEPS.address);
    }
*/
    getFormData(): FormData {
        // Return the entire Form Data
        return this.formData;
    }

    resetFormData(): FormData {
        // Reset the workflow
        this.workflowService.resetSteps();
        // Return the form data after all this.* members had been reset
        this.formData.clear();
        this.isPersonalFormValid = this.isCompanyFormValid = this.isWorkFormValid = this.isAddressFormValid = false;
        return this.formData;
    }

    isFormValid() {
        // Return true if all forms had been validated successfully; otherwise, return false
        return this.isPersonalFormValid && this.isCompanyFormValid && this.isWorkFormValid && this.isAddressFormValid;
    }
}
