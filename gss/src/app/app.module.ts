import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {DndModule} from 'ng2-dnd';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

import { AppComponent } from './app.component';
import { CompanyComponent } from './company/company.component';
import { FacilityComponent } from './facility/facility.component';
import { EmployeeComponent } from './employee/employee.component';
import { RegistrationComponent } from './facility/registration/registration.component';
import { AdminInfoComponent } from './facility/registration/admin-info/admin-info.component';
import { FacilityInfoComponent } from './facility/registration/facility-info/facility-info.component';
import { CoursesComponent } from './facility/registration/courses/courses.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './company/register/register.component';
import { CreateAccountComponent } from './company/register/create-account/create-account.component';
import { CreateRolesComponent } from './company/register/create-roles/create-roles.component';
import { AddWorkersComponent } from './company/register/add-workers/add-workers.component';
import { EmployeeRegComponent } from './employee/employee-reg/employee-reg.component';
import { UserInfoComponent } from './employee/employee-reg/user-info/user-info.component';
import { AddTrainingCertComponent } from './employee/employee-reg/add-training-cert/add-training-cert.component';
import { SchedTrainingComponent } from './employee/employee-reg/sched-training/sched-training.component';
import { CnfTrainingComponent } from './employee/employee-reg/cnf-training/cnf-training.component';

/* Shared Service */
import { FormDataService } from './data/formData.service';
import { WorkflowGuard } from './workflow/workflow-guard.service';
import { WorkflowService } from './workflow/workflow.service';
import { EmployeeProfileComponent } from './employee/employee-profile/employee-profile.component';
import { PublicComponent } from './employee/employee-profile/public/public.component';
import { PrivateComponent } from './employee/employee-profile/private/private.component';
import { SidenavMatComponent } from './sidenav-mat/sidenav-mat.component';
import { LayoutModule } from '@angular/cdk/layout';
import { CompanyProfileComponent } from './company/register/company-profile/company-profile.component';
import {AddRoleComponent} from './company/register/create-roles/add-role/add-role.component';
import { CompanyNewjobComponent } from './company/company-newjob/company-newjob.component';
import { ClientComponent } from './client/client.component';
import { SelectactivityComponent } from './client/selectactivity/selectactivity.component';
import { SelectlocationComponent } from './client/selectlocation/selectlocation.component';
import { DescriptionComponent } from './client/description/description.component';
import { NewjobRolesComponent } from './client/newjob-roles/newjob-roles.component';

export const appRoutes: Routes = [
  { path: 'company/register/company-profile', component: CompanyProfileComponent },
  { path: 'company/company-newjob', component: CompanyNewjobComponent },
  { path: 'client', component: ClientComponent },
  { path: 'facility', component: FacilityComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'employee/employee-profile/public', component: PublicComponent },
  { path: 'employee/employee-profile/private', component: PrivateComponent },
  { path: 'company/register/create-roles', component: CreateRolesComponent},
  { path: 'company', component: CompanyComponent, canActivate: [WorkflowGuard] },
  { path: 'facility', component: FacilityComponent, canActivate: [WorkflowGuard] },
  { path: 'personal', component: CreateAccountComponent, canActivate: [WorkflowGuard] },
  { path: 'work', component: CreateRolesComponent, canActivate: [WorkflowGuard] },
  { path: 'company-data', component: CompanyProfileComponent, canActivate: [WorkflowGuard] }
];

@NgModule({
  declarations: [
    AppComponent,
    CompanyComponent,
    FacilityComponent,
    EmployeeComponent,
    RegistrationComponent,
    AdminInfoComponent,
    FacilityInfoComponent,
    CoursesComponent,
    RegisterComponent,
    CreateAccountComponent,
    AddRoleComponent,
    CreateRolesComponent,
    AddWorkersComponent,
    EmployeeRegComponent,
    UserInfoComponent,
    AddTrainingCertComponent,
    SchedTrainingComponent,
    CnfTrainingComponent,
    EmployeeProfileComponent,
    PublicComponent,
    PrivateComponent,
    SidenavMatComponent,
    CompanyProfileComponent,
    CompanyNewjobComponent,
    ClientComponent,
    SelectactivityComponent,
    SelectlocationComponent,
    DescriptionComponent,
    NewjobRolesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    DndModule.forRoot(),
    Ng2SearchPipeModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserAnimationsModule,
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule, MatBadgeModule, MatBottomSheetModule, MatButtonModule, MatButtonToggleModule,
    MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatDividerModule,
    MatExpansionModule, MatGridListModule, MatIconModule,  MatInputModule, MatListModule, MatMenuModule,
    MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule,
    MatRippleModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule,
    MatSortModule, MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule, LayoutModule
  ],
  entryComponents: [
    CreateRolesComponent, AddRoleComponent
  ],
  exports: [ CreateRolesComponent, AddRoleComponent ],
  providers:    [{ provide: FormDataService, useClass: FormDataService },
    { provide: WorkflowService, useClass: WorkflowService }, WorkflowGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
