import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewjobRolesComponent } from './newjob-roles.component';

describe('NewjobRolesComponent', () => {
  let component: NewjobRolesComponent;
  let fixture: ComponentFixture<NewjobRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewjobRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewjobRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
