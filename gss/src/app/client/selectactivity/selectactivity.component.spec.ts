import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectactivityComponent } from './selectactivity.component';

describe('SelectactivityComponent', () => {
  let component: SelectactivityComponent;
  let fixture: ComponentFixture<SelectactivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectactivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectactivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
