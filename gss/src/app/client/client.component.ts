import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  isActivity: Boolean;
  isLocation: Boolean;
  isDescription: Boolean;
  isRoles: Boolean;
  rightClass: String;
  activityClass: String;
  locationClass: String;
  constructor() { }

  ngOnInit() {
    this.isActivity = true;
    this.rightClass = 'col-md-7';
    this.isRoles = false;
  }
  addActivity() {
    this.setDefault();
    this.isActivity = true;
    this.activityClass = 'col-md-4';
  }
  addDescription() {
    this.setDefault();
    this.isDescription = true;
  }
  addLocation() {
    this.setDefault();
    this.isLocation = true;
    this.locationClass = 'col-md-4';
  }
  addRoles() {
    this.setDefault();
    this.isRoles = true;
    this.rightClass = 'col-md-7';
  }
  setDefault() {
    this.isActivity = false;
    this.isLocation = false;
    this.isDescription = false;
    this.isRoles = false;
  }
}
