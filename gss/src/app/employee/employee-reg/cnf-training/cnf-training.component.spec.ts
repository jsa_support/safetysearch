import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnfTrainingComponent } from './cnf-training.component';

describe('CnfTrainingComponent', () => {
  let component: CnfTrainingComponent;
  let fixture: ComponentFixture<CnfTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnfTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnfTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
