import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrainingCertComponent } from './add-training-cert.component';

describe('AddTrainingCertComponent', () => {
  let component: AddTrainingCertComponent;
  let fixture: ComponentFixture<AddTrainingCertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTrainingCertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTrainingCertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
