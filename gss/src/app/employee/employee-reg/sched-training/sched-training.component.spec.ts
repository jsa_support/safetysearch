import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedTrainingComponent } from './sched-training.component';

describe('SchedTrainingComponent', () => {
  let component: SchedTrainingComponent;
  let fixture: ComponentFixture<SchedTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
