import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  course: any;
  facility: string;
  duration: any;
  credits: any;
}

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.css']
})
export class PrivateComponent implements OnInit {
  displayedColumns = ['course', 'facility', 'credits', 'duration'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
const ELEMENT_DATA: PeriodicElement[] = [
  {course: 'course name', facility: 'facility name', credits: '10 hrs', duration: 'June 11 - June 21'},
  {course: 'course name', facility: 'facility name', credits: '10 hrs', duration: 'June 11 - June 21'},
  {course: 'course name', facility: 'facility name', credits: '10 hrs', duration: 'June 11 - June 21'},
];
