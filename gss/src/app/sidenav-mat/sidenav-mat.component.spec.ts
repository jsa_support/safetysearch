
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SidenavMatComponent } from './sidenav-mat.component';

describe('SidenavMatComponent', () => {
  let component: SidenavMatComponent;
  let fixture: ComponentFixture<SidenavMatComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatSidenavModule],
      declarations: [SidenavMatComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SidenavMatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
