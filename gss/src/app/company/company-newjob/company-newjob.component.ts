import { Component, OnInit } from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material';

export interface PeriodicElement {
  employee: string;
  role: string;
  training: string;
  received: any;
  expires: any;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {employee: 'Full Name', role: 'Hydrogen', training: 'Expired', received: '15-07-2018', expires: '15-07-2018'},
  {employee: 'Full Name', role: 'Helium', training: 'Expired', received: '15-07-2018', expires: '15-07-2018'},
  {employee: 'Full Name', role: 'Lithium', training: 'Active', received: '15-07-2018', expires: '15-07-2018'},
  {employee: 'Full Name', role: 'Beryllium', training: 'Expired', received: '15-07-2018', expires: '15-07-2018'},
  {employee: 'Full Name', role: 'Boron', training: 'In Pending', received: '15-07-2018', expires: '15-07-2018'},
  {employee: 'Full Name', role: 'Carbon', training: 'Active', received: '15-07-2018', expires: '15-07-2018'},
  {employee: 'Full Name', role: 'Neon', training: 'Expired', received: '15-07-2018', expires: '15-07-2018 '},
];


@Component({
  selector: 'app-company-newjob',
  templateUrl: './company-newjob.component.html',
  styleUrls: ['./company-newjob.component.css']
})
export class CompanyNewjobComponent implements OnInit {
  displayedColumns: string[] = ['select', 'employee', 'role', 'training', 'received', 'expires'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  constructor() { }

  ngOnInit() {
  }

}
