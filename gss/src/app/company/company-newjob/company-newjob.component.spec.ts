import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyNewjobComponent } from './company-newjob.component';

describe('CompanyNewjobComponent', () => {
  let component: CompanyNewjobComponent;
  let fixture: ComponentFixture<CompanyNewjobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyNewjobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyNewjobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
