import { Component, OnInit } from '@angular/core';
import { Company} from '../../../data/formData.model';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {FormDataService} from '../../../data/formData.service';


@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {
  companyData : Company;
  form: any;
  industryControl = new FormControl('', [Validators.required]);
  industries: Industry[] = [
    {value: 'construction', viewValue: 'Construction '},
    {value: 'oil', viewValue: 'Oil & Gas '},
    {value: 'concrete', viewValue: 'Concrete'},
    {value: 'landscaper', viewValue: 'Landscaper'},
    {value: 'electrician', viewValue: 'Electrician'},
    {value: 'crane', viewValue: 'Crane'},
    {value: 'carpenter', viewValue: 'Carpenter'},
  ];
  company: Size[] = [
    {value: 'steak-0', viewValue: '10+'},
    {value: 'pizza-1', viewValue: '50+'},
    {value: 'tacos-2', viewValue: '100+'},
    {value: 'tacos-2', viewValue: '500+'},
    {value: 'tacos-2', viewValue: '1000+'},
    {value: 'tacos-2', viewValue: '2000+'},
    {value: 'tacos-2', viewValue: '5000+'}
  ];
  constructor(private router: Router, private formDataService: FormDataService ) { }

  ngOnInit() {
    this.companyData = this.formDataService.getCompany();
    console.log('CompanyData feature loaded!');
  }
  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }

    this.formDataService.setCompany(this.companyData);
    return true;
  }
  goToPrevious(form: any) {
    //if (this.save(form)) {
      // Navigate to the personal page
      this.router.navigate(['/personal']);
    //}
  }
  goToNext(form: any) {
    if (this.save(form)) {
      // Navigate to the address page
      this.router.navigate(['/work']);
    }
  }
}
export interface Industry {
  value: string;
  viewValue: string;
}
export interface Size {
  value: any;
  viewValue: any;
}