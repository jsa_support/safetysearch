import {Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import { Work } from '../../../data/formData.model';
import { FormDataService } from '../../../data/formData.service';
import {AddRoleComponent} from './add-role/add-role.component';

@Component({
  selector: 'app-create-roles',
  templateUrl: './create-roles.component.html',
  styleUrls: ['./create-roles.component.css']
})

export class CreateRolesComponent implements OnInit {
  [x: string]: any;

  title = 'What do you do?';
  work: Work;
  form: any;
  searchCourse: any;
  availableCredits: Array<Credit> = [];
  availableCreditCategories: Array<CreditCategory> = [];
  roles: Array<Role> = [];
  selectedCredit: any;
  panelOpenState = false;
  allExpandState = false;
  constructor(private router: Router, private formDataService: FormDataService, public dialog: MatDialog) {
    this.availableCreditCategories.push(new CreditCategory(1, 'Foreman', []));
    this.availableCreditCategories.push(new CreditCategory(2, 'Laborer', []));
    this.availableCreditCategories.push(new CreditCategory(3, 'Engineer', []));
    this.availableCreditCategories.push(new CreditCategory(4, 'Observer', []));
    this.availableCreditCategories.push(new CreditCategory(5, 'Electrician', []));
    this.availableCreditCategories.push(new CreditCategory(6, 'Crane', []));
    this.availableCreditCategories.push(new CreditCategory(7, 'Carpenter', []));
    this.availableCreditCategories.push(new CreditCategory(8, 'Excavator', []));

    this.availableCreditCategories[0].CCItems.push(new Credit(111, 'OSHA 10', 'Description of element', 10));
    this.availableCreditCategories[0].CCItems.push(new Credit(112, 'OSHA 30', 'Description of element', 30));
    this.availableCreditCategories[0].CCItems.push(new Credit(113, 'OSHA 20', 'Description of element', 20));

    this.availableCreditCategories[1].CCItems.push(new Credit(114, 'OSHA 10', 'Description of element', 10));
    this.availableCreditCategories[1].CCItems.push(new Credit(115, 'OSHA 30', 'Description of element', 30));
    this.availableCreditCategories[1].CCItems.push(new Credit(116, 'OSHA 40', 'Description of element', 40));

    this.availableCreditCategories[2].CCItems.push(new Credit(117, 'OSHA 10', 'Description of element', 10));

    this.availableCreditCategories[3].CCItems.push(new Credit(118, 'OSHA 10', 'Description of element', 10));

    this.availableCreditCategories[4].CCItems.push(new Credit(119, 'OSHA 10', 'Description of element', 10));

    this.availableCreditCategories[5].CCItems.push(new Credit(120, 'OSHA 10', 'Description of element', 10));

    this.availableCreditCategories[6].CCItems.push(new Credit(121, 'OSHA 20', 'Description of element', 20));

    this.availableCreditCategories[7].CCItems.push(new Credit(122, 'OSHA 20', 'Description of element', 30));

    this.roles.push(new Role(8, 'Foreman', []));
    this.roles.push(new Role(9, 'Laborer', []));
    this.roles.push(new Role(10, 'Engineer', []));

    this.roles[0].RItems.push(new Credit(114, 'OSHA 10', 'Description of element', 10));
    this.roles[1].RItems.push(new Credit(114, 'OSHA 10', 'Description of element', 10));
    this.roles[2].RItems.push(new Credit(114, 'OSHA 10', 'Description of element', 10));

    this.availableCredits.push(new Credit(1, 'Blue Shoes', '1', 35));
    this.availableCredits.push(new Credit(2, 'Good Jacket', '1', 90));
    this.availableCredits.push(new Credit(3, 'Red Shirt', '5', 12));
    this.availableCredits.push(new Credit(4, 'Blue Jeans', '4', 60));
    this.selectedCredit = [];
  }

  ngOnInit() {
    this.work = this.formDataService.getWork();
    this.work.searchCourse = ' ';
    console.log('Work feature loaded!');
  }
  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }

    this.formDataService.setWork(this.work);
    return true;
  }
  goToPrevious(form: any) {
    // if (this.save(form)) {
    // Navigate to the personal page
    this.router.navigate(['/company-data']);
    // }
  }
  goToNext(form: any) {
    if (this.save(form)) {
      // Navigate to the address page
      this.router.navigate(['/address']);
    }
  }


  displayCreditInfo(creditDetails: Credit) {
    // console.log(creditDetails);
    this.selectedCredit = creditDetails;
  }
  addToRoleCourses($event: any, cid: number) {
    const newCredit: CreditCategory = $event.dragData;
    if ( newCredit.CCItems.length) {
      for (let i = 0; i < newCredit.CCItems.length; i++) {
        // console.log("each credit", newCredit.CCItems[i]);
        this.roles[cid].RItems.push(newCredit.CCItems[i]);
      }
    }
    // console.log("new credit",newCredit.CCItems);
  }
  openAddRoleDialog(): void {
    const dialogRef = this.dialog.open(AddRoleComponent, {
      height: '300px',
      width: '350px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
}
export interface DialogData {
  animal: string;
  name: string;
}
class CreditCategory {
  constructor( public CCId: number, public CCTitle: string, public CCItems: Array<Credit> ) {}
}
class Role {
  constructor( public RId: number, public Rtitle: string, public RItems: Array<Credit> ) {}
}
class Credit {
  constructor(public crediId: number, public name: string, public description: string, public credits: number) {}
}
