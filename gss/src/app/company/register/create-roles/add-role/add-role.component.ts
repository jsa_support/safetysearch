import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {DialogData} from '../create-roles.component';

export interface Type {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.css']
})
export class AddRoleComponent implements OnInit {
  typeControl = new FormControl('', [Validators.required]);
  types: Type[] = [
    {value: 'company', viewValue: 'Company'},
    {value: 'role', viewValue: 'Role'},
    {value: 'jobsite', viewValue: 'JobSite'}
  ];
  constructor( public dialogRef: MatDialogRef<AddRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
